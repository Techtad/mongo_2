var http = require("http")
var fs = require("fs")
var SocketIO = require("socket.io")
var mongodb = require("mongodb")
var MongoClient = mongodb.MongoClient
var ObjectID = mongodb.ObjectID
var Operations = require("./modules/operations.js")
var ops = new Operations()

function sendFile(fileName, res) {
    if (fileName == "/") fileName = "/static/index.html"
    fs.readFile(`${__dirname}/${fileName}`, function (error, data) {
        if (error) {
            console.log(error)
            res.writeHead(404, { "Content-Type": "text/html;charset=utf-8" })
            res.end(`Błąd 404: Nie znaleziono pliku ${fileName}`)
        } else {
            let nameParts = fileName.split(".")
            let ext = nameParts[nameParts.length - 1].toLowerCase()
            let contentType = "text/html;charset=utf-8"
            switch (ext) {
                case "js":
                    contentType = "application/javascript"
                    break
                case "json":
                    contentType = "application/json"
                    break
                case "css":
                    contentType = "text/css"
                    break
                case "jpg": case "jpeg":
                    contentType = "image/jpeg"
                    break
                case "mp3":
                    contentType = "audio/mpeg"
                    break
                case "png":
                    contentType = "image/png"
                    break
                case "svg":
                    contentType = "image/svg+xml"
                    break
                case "gif":
                    contentType = "image/gif"
                    break
                case "txt":
                    contentType = "text/plain"
                    break
                default: break
            }
            res.writeHead(200, { "Content-Type": contentType })
            res.end(data)
        }
    })
}

function postRepsonse(req, res) {
    let allData = ""
    req.on("data", function (data) {
        allData += data
    })
    req.on("end", function (data) {
        console.log("Data(" + req.url + "):" + allData)
        let obj
        if (allData) {
            try {
                obj = JSON.parse(allData)
            } catch (error) {
                console.log(error)
                res.writeHead(400, { "Content-Type": "text/html;charset=utf-8" })
                res.end(error)
            }
        }
        switch (req.url) {
            default:
                console.log("Błędny POST: " + req.url)
                res.writeHead(400, { "Content-Type": "application/json;charset=utf-8" })
                res.end("Błędna akcja POST: " + req.url)
                break
        }
    })
}

var server = http.createServer(function (req, res) {
    console.log(req.method + " : " + req.url)
    switch (req.method) {
        case "GET":
            sendFile(decodeURI(req.url).split('?')[0], res)
            break;
        case "POST":
            postRepsonse(req, res)
            break;
    }
})

const port = 3000
server.listen(3000, function () {
    console.log("Start serwera na porcie " + port)
})


var _db = null
var _admin = null
var connected = false
var dbList = []

var SocketServer = SocketIO.listen(server)
console.log("Start Socket.io")

SocketServer.on("connection", function (client) {
    console.log("Klient się połączył:", client.id)
    client.on("disconnect", function () {
        if (_db) _db.close()
        _db = null
        _admin = null
        connected = false
        dbList = []
        console.log("Klient się rozłączył:", this.id)
    })

    client.on("mongo_connect", function (data) {
        if (!connected && data.address) {
            MongoClient.connect(`mongodb://${data.address}`, function (err, db) {
                if (err) {
                    console.log(err)
                    client.emit("mongo_connect_resp", { success: false })
                }
                else {
                    connected = true
                    console.log(`Mongo podłączone na adresie ${data.address}`)
                    _db = db
                    _admin = db.admin()
                    _admin.listDatabases(function (err, dbs) {
                        if (err) console.log(err)
                        else {
                            console.log(dbs.databases)
                            console.log(dbs)
                        }

                    })
                    client.emit("mongo_connect_resp", { success: true })
                }
            })
        } else {
            client.emit("mongo_connect_resp", { success: false })
        }
    })
    client.on("list_dbs", function (data) {
        _admin.listDatabases(function (err, dbs) {
            if (err) {
                console.log(err)
                client.emit("list_dbs_resp", { success: false, err: err })
            }
            else {
                let dbNames = []
                for (let item of dbs.databases) {
                    if (item.name != "admin" && item.name != "local" && item.name != "config")
                        dbNames.push(item.name)
                }
                client.emit("list_dbs_resp", { success: true, dbNames: dbNames })
            }
        })
    })
    client.on("list_colls", function (data) {
        MongoClient.connect(`mongodb://${data.address}/${data.dbName}`, function (err, db) {
            if (err) {
                console.log(err)
                client.emit("list_colls_resp", { success: false, err: err })
            } else {
                db.listCollections().toArray(function (err, collInfos) {
                    if (err) {
                        console.log(err)
                        client.emit("list_colls_resp", { success: false, err: err })
                    } else {
                        let collNames = []
                        for (let inf of collInfos) collNames.push(inf.name)
                        client.emit("list_colls_resp", { success: true, collNames: collNames })
                    }
                    db.close()
                })
            }
        })
    })
    client.on("create_db", function (data) {
        MongoClient.connect(`mongodb://${data.address}/${data.dbName}`, function (err, db) {
            if (err) {
                console.log(err)
                client.emit("create_db_resp", { success: false, err: err })
            } else {
                db.createCollection("defaultCollection", function (err, coll) {
                    if (err) {
                        console.log(err)
                        client.emit("create_db_resp", { success: false, err: err })
                    } else
                        client.emit("create_db_resp", { success: true })
                    db.close()
                })
            }
        })
    })
    client.on("create_coll", function (data) {
        MongoClient.connect(`mongodb://${data.address}/${data.dbName}`, function (err, db) {
            if (err) {
                console.log(err)
                client.emit("create_coll_resp", { success: false, err: err })
            } else {
                db.createCollection(data.collName, function (err, coll) {
                    if (err) {
                        console.log(err)
                        client.emit("create_coll_resp", { success: false, err: err })
                    } else
                        client.emit("create_coll_resp", { success: true })
                    db.close()
                })
            }
        })
    })
    client.on("delete_db", function (data) {
        MongoClient.connect(`mongodb://${data.address}/${data.dbName}`, function (err, db) {
            if (err) {
                console.log(err)
                client.emit("delete_db_resp", { success: false, err: err })
            } else {
                db.dropDatabase(function (err, result) {
                    if (err) {
                        console.log(err)
                        client.emit("delete_db_resp", { success: false, err: err })
                    } else
                        client.emit("delete_db_resp", { success: true })
                    db.close();
                })
            }
        })
    })
    client.on("delete_coll", function (data) {
        MongoClient.connect(`mongodb://${data.address}/${data.dbName}`, function (err, db) {
            if (err) {
                console.log(err)
                client.emit("delete_coll_resp", { success: false, err: err })
            } else {
                db.collection(data.collName).drop(function (err, delOK) {
                    if (err) {
                        console.log(err)
                        client.emit("delete_coll_resp", { success: false, err: err })
                    } else
                        client.emit("delete_coll_resp", { success: true })
                    db.close()
                })
            }
        })
    })
    client.on("load_coll", function (data) {
        MongoClient.connect(`mongodb://${data.address}/${data.dbName}`, function (err, db) {
            if (err) {
                console.log(err)
                client.emit("load_coll_resp", { success: false, err: err })
            } else {
                let coll = db.collection(data.collName)
                ops.SelectAll(coll, function (err, items) {
                    if (err) {
                        console.log(err)
                        client.emit("load_coll_resp", { success: false, err: err })
                    } else
                        client.emit("load_coll_resp", { success: true, items: items })
                    db.close()
                })
            }
        })
    })

    client.on("add_record", function (data) {
        MongoClient.connect(`mongodb://${data.address}/${data.dbName}`, function (err, db) {
            if (err) {
                console.log(err)
                client.emit("add_record_resp", { success: false, err: err })
            } else {
                let coll = db.collection(data.collName)
                ops.Insert(coll, {}, function (err, result) {
                    if (err) {
                        console.log(err)
                        client.emit("add_record_resp", { success: false, err: err })
                    } else
                        client.emit("add_record_resp", { success: true, result: result })
                    db.close()
                })
            }
        })
    })
    client.on("delete_record", function (data) {
        MongoClient.connect(`mongodb://${data.address}/${data.dbName}`, function (err, db) {
            if (err) {
                console.log(err)
                client.emit("delete_record_resp", { success: false, err: err })
            } else {
                let coll = db.collection(data.collName)
                ops.DeleteById(ObjectID, coll, data.id, function (err, result) {
                    if (err) {
                        console.log(err)
                        client.emit("delete_record_resp", { success: false, err: err })
                    } else
                        client.emit("delete_record_resp", { success: true, result: result })
                    db.close()
                })
            }
        })
    })
    client.on("edit_record", function (data) {
        MongoClient.connect(`mongodb://${data.address}/${data.dbName}`, function (err, db) {
            if (err) {
                console.log(err)
                client.emit("edit_record_resp", { success: false, err: err, item_id: data.id })
            } else {
                let coll = db.collection(data.collName)
                ops.UpdateById(ObjectID, coll, data.id, data.item, function (err, result) {
                    if (err) {
                        console.log(err)
                        client.emit("edit_record_resp", { success: false, err: err, item_id: data.id })
                    } else
                        client.emit("edit_record_resp", { success: true, result: result, item_id: data.id })
                    db.close()
                })
            }
        })
    })
})