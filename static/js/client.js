var Socket = new io()
var MongoAddress
var Connected = false
var SelectedDB = null
var SelectedColl = null

addEventListener("DOMContentLoaded", function (event) {
    eventsAndResponses()
    start()
})

function eventsAndResponses() {
    Socket.on("mongo_connect_resp", function (data) {
        console.log("mongo_connect", data)
        if (Connected) return
        if (data.success) {
            Connected = true
            alert(`Połączono z serwerem MongoDB pod adresem: ${MongoAddress}`)
            initUI()
        }
        else if (confirm(`Nieudane połączenie z: ${MongoAddress}\nPołączyć się zamiast tego z: localhost?`)) {
            MongoAddress = "localhost"
            Socket.emit("mongo_connect", { address: MongoAddress })
        }
    })

    Socket.on("list_dbs_resp", function (data) {
        console.log("list_dbs", data)
        if (data.success && data.dbNames) {
            $("#dbs_list").empty()
            for (let name of data.dbNames) {
                let div = $("<div>")
                div.text(name)
                div.addClass("db_name")
                $("#dbs_list").append(div)
            }
            $(".db_name").each(function () {
                $(this).on("click", function (event) {
                    if (SelectedDB != $(this)) {
                        if (SelectedDB) SelectedDB.removeClass("db_selected")
                        SelectedDB = $(this)
                        $(this).addClass("db_selected")
                    }
                    if (SelectedDB) {
                        Socket.emit("list_colls", { address: MongoAddress, dbName: SelectedDB.text() })
                        $("#selected-db").text(`/${SelectedDB.text()}`)
                    }
                    if (SelectedColl) $("#btn_add_record").removeAttr("disabled")
                    else $("#btn_add_record").attr("disabled", "")
                })
            })
        } else
            alert(JSON.stringify(data.err, null, 2))
    })
    Socket.on("list_colls_resp", function (data) {
        console.log("list_colls", data)
        if (data.success && data.collNames) {
            $("#colls_list").empty()
            for (let name of data.collNames) {
                let div = $("<div>")
                div.text(name)
                div.addClass("coll_name")
                $("#colls_list").append(div)
            }
            $(".coll_name").each(function () {
                $(this).on("click", function (event) {
                    if (SelectedColl != $(this)) {
                        if (SelectedColl) SelectedColl.removeClass("coll_selected")
                        SelectedColl = $(this)
                        $(this).addClass("coll_selected")
                    }
                    if (SelectedDB && SelectedColl) {
                        Socket.emit("load_coll", { address: MongoAddress, dbName: SelectedDB.text(), collName: SelectedColl.text() })
                        $("#selected-coll").text(`/${SelectedDB.text()}/${SelectedColl.text()}`)
                        $("#btn_add_record").removeAttr("disabled")
                    } else $("#btn_add_record").attr("disabled", "")
                })
            })
        } else
            alert(JSON.stringify(data.err, null, 2))
    })

    Socket.on("create_db_resp", function (data) {
        console.log("create_db", data)
        if (data.success) {
            //alert("Dodano bazę danych")
            Socket.emit("list_dbs")
        } else
            alert(JSON.stringify(data.err, null, 2))
    })
    Socket.on("create_coll_resp", function (data) {
        console.log("create_coll", data)
        if (data.success) {
            //alert("Dodano kolekcję")
            if (SelectedDB) Socket.emit("list_colls", { address: MongoAddress, dbName: SelectedDB.text() })
        } else
            alert(JSON.stringify(data.err, null, 2))
    })

    Socket.on("delete_db_resp", function (data) {
        console.log("delete_db", data)
        if (data.err) alert(JSON.stringify(data.err, null, 2))
        Socket.emit("list_dbs")
    })
    Socket.on("delete_coll_resp", function (data) {
        console.log("delete_coll", data)
        if (data.err) alert(JSON.stringify(data.err, null, 2))
        if (SelectedDB) Socket.emit("list_colls", { address: MongoAddress, dbName: SelectedDB.text() })
    })

    Socket.on("load_coll_resp", function (data) {
        console.log("load_coll", data)
        if (data.err) alert(JSON.stringify(data.err, null, 2))
        $("#records").empty()
        for (let item of data.items) {
            let div = $("<div>")
            div.addClass("item")
            div.attr("item_id", item._id)

            let textarea = $("<textarea>")
            textarea.attr("disabled", "")
            textarea.text(JSON.stringify(item, null, 0))
            div.append(textarea)

            let editBtn = $("<input type='button'>")
            editBtn.attr("value", "Edytuj")
            editBtn.on("click", function (event) {
                let ta = $(this).parent().find("textarea")
                let edit = $(this)
                let del = $(this).parent().find("input[value='Usuń']")
                let accept = $(this).parent().find("input[value='Zatwierdź']")
                let cancel = $(this).parent().find("input[value='Anuluj']")

                ta.removeAttr("disabled")
                edit.css("display", "none")
                del.css("display", "none")
                accept.css("display", "")
                cancel.css("display", "")
            })
            div.append(editBtn)

            let deleteBtn = $("<input type='button'>")
            deleteBtn.attr("value", "Usuń")
            deleteBtn.on("click", function (event) {
                if (SelectedDB && SelectedColl) Socket.emit("delete_record", { address: MongoAddress, dbName: SelectedDB.text(), collName: SelectedColl.text(), id: $(this).parent().attr("item_id") })
            })
            div.append(deleteBtn)

            let acceptBtn = $("<input type='button'>")
            acceptBtn.attr("value", "Zatwierdź")
            acceptBtn.on("click", function (event) {
                let ta = $(this).parent().find("textarea")
                try {
                    let item = JSON.parse(ta.val())
                    if (item._id) delete item._id
                    if (SelectedDB && SelectedColl) Socket.emit("edit_record", { address: MongoAddress, dbName: SelectedDB.text(), collName: SelectedColl.text(), id: $(this).parent().attr("item_id"), item: item })
                } catch (e) {
                    ta.css("background-color", "red")
                }
            })
            acceptBtn.css("display", "none")
            div.append(acceptBtn)

            let cancelBtn = $("<input type='button'>")
            cancelBtn.attr("value", "Anuluj")
            cancelBtn.on("click", function (event) {
                if (SelectedDB && SelectedColl) Socket.emit("load_coll", { address: MongoAddress, dbName: SelectedDB.text(), collName: SelectedColl.text() })

            })
            cancelBtn.css("display", "none")
            div.append(cancelBtn)

            $("#records").append(div)
        }
    })

    Socket.on("add_record_resp", function (data) {
        console.log("add_record", data)
        if (data.err) alert(JSON.stringify(data.err, null, 2))
        if (SelectedDB && SelectedColl) Socket.emit("load_coll", { address: MongoAddress, dbName: SelectedDB.text(), collName: SelectedColl.text() })
    })
    Socket.on("delete_record_resp", function (data) {
        console.log("delete_record", data)
        if (data.err) alert(JSON.stringify(data.err, null, 2))
        if (SelectedDB && SelectedColl) Socket.emit("load_coll", { address: MongoAddress, dbName: SelectedDB.text(), collName: SelectedColl.text() })
    })
    Socket.on("edit_record_resp", function (data) {
        console.log("edit_record", data)
        if (data.err) alert(JSON.stringify(data.err, null, 2))
        if (SelectedDB && SelectedColl) Socket.emit("load_coll", { address: MongoAddress, dbName: SelectedDB.text(), collName: SelectedColl.text() })
    })

    $("#btn_refresh_dbs").on("click", function (event) {
        Socket.emit("list_dbs")
    })
    $("#btn_refresh_colls").on("click", function (event) {
        if (SelectedDB) Socket.emit("list_colls", { address: MongoAddress, dbName: SelectedDB.text() })
    })

    $("#btn_create_db").on("click", function (event) {
        let dbName = prompt("Wprowadź nazwę nowej bazy danych")
        Socket.emit("create_db", { address: MongoAddress, dbName: dbName })
    })
    $("#btn_create_coll").on("click", function (event) {
        if (!SelectedDB) return
        let collName = prompt("Wprowadź nazwę nowej kolekcji")
        Socket.emit("create_coll", { address: MongoAddress, dbName: SelectedDB.text(), collName: collName })
    })

    $("#btn_delete_db").on("click", function (event) {
        if (SelectedDB) Socket.emit("delete_db", { address: MongoAddress, dbName: SelectedDB.text() })
    })
    $("#btn_delete_coll").on("click", function (event) {
        if (SelectedDB && SelectedColl) Socket.emit("delete_coll", { address: MongoAddress, dbName: SelectedDB.text(), collName: SelectedColl.text() })
    })

    $("#btn_add_record").on("click", function (event) {
        if (SelectedDB && SelectedColl) Socket.emit("add_record", { address: MongoAddress, dbName: SelectedDB.text(), collName: SelectedColl.text() })
    })
}

function start() {
    MongoAddress = prompt("Podaj adres serwera MongoDB")
    Socket.emit("mongo_connect", { address: MongoAddress })
}

function initUI() {
    $("#app_header").html(`Panel administracyjny serwera MongoDB pod adresem <span style='color:red'>${MongoAddress}</span>`)
    Socket.emit("list_dbs")
}